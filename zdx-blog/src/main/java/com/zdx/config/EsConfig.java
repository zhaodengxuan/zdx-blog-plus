package com.zdx.config;


import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.JsonpMapper;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.zdx.config.properties.EsProperties;
import com.zdx.search.EsSearchTemplateImpl;
import com.zdx.search.SearchTemplate;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(EsProperties.class)
@ConditionalOnExpression("'${zdx.es.open}' == 'true'")
public class EsConfig {

    @Autowired
    private EsProperties esProperties;

    @Bean
    public ElasticsearchClient restClient(JsonpMapper jsonpMapper) {
        RestClientBuilder builder = RestClient.builder(HttpHost.create(esProperties.getUrl()));
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(esProperties.getUsername(), esProperties.getPassword()));
        builder.setHttpClientConfigCallback(f -> f.setDefaultCredentialsProvider(credentialsProvider));
        return new ElasticsearchClient(new RestClientTransport(builder.build(), jsonpMapper));
    }

    @Bean
    public SearchTemplate searchTemplate() {
        return new EsSearchTemplateImpl();
    }

}
