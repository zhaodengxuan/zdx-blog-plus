package com.zdx.search;


import cn.hutool.core.util.ObjUtil;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.AcknowledgedResponse;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch.core.*;
import co.elastic.clients.elasticsearch.core.bulk.BulkOperation;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.core.search.HitsMetadata;
import co.elastic.clients.elasticsearch.indices.CreateIndexRequest;
import co.elastic.clients.elasticsearch.indices.CreateIndexResponse;
import co.elastic.clients.elasticsearch.indices.DeleteIndexRequest;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.*;


@Slf4j
@SuppressWarnings("all")
public class EsSearchTemplateImpl implements SearchTemplate {

    @Resource
    private ElasticsearchClient elasticsearchClient;

    @Override
    public Boolean createIndex(String index) {
        if (isIndexExist(index)) {
            log.error("索引不存在");
            return false;
        }
        CreateIndexRequest request = new CreateIndexRequest.Builder()
                .index(index)
                .settings(s -> s.analysis(a -> a.analyzer("ik_analyzer", an -> an.custom(c -> c.tokenizer("ik_max_word")))))
                .build();
        try {
            CreateIndexResponse response = elasticsearchClient.indices().create(request);
            return response.acknowledged();
        } catch (IOException e) {
            log.error("创建索引出现错误：{}", e.getMessage());
            return false;
        }
    }

    @Override
    public Boolean deleteIndex(String index) {
        if (isIndexExist(index)) {
            log.error("索引不存在");
            return false;
        }
        DeleteIndexRequest request = new DeleteIndexRequest.Builder().index(index).build();
        try {
            AcknowledgedResponse response = elasticsearchClient.indices().delete(request);
            return response.acknowledged();
        } catch (IOException e) {
            log.error("删除索引异常：{}", e.getMessage());
            return false;
        }
    }

    @Override
    public Boolean isIndexExist(String index) {
        co.elastic.clients.elasticsearch.indices.ExistsRequest request = new co.elastic.clients.elasticsearch.indices.ExistsRequest.Builder().index(index).build();
        try {
            return elasticsearchClient.indices().exists(request).value();
        } catch (IOException e) {
            log.error("查询索引出错：{}", e.getMessage());
            return false;
        }
    }

    @Override
    public void insertDoc(Object object, String index, String id) {
        IndexRequest<?> request = IndexRequest.of(re -> re.index(index).id(id)
                .document(object));
        try {
            IndexResponse response = elasticsearchClient.index(request);
            log.info("新增文档成功：{}", response);
        } catch (IOException e) {
            log.error("查询文档异常：{}", e.getMessage());
        }
    }

    @Override
    public void deleteDoc(String index, String id) {
        DeleteRequest request = DeleteRequest.of(re -> re.index(index).id(id));
        try {
            DeleteResponse response = elasticsearchClient.delete(request);
            log.info("删除文档成功：{}", response);
        } catch (IOException e) {
            log.error("删除文档异常：{}", e.getMessage());
        }
    }

    @Override
    public void updateDoc(Object data, String index, String id) {
        UpdateRequest<?, ?> request = UpdateRequest.of(re -> re.index(index).id(id)
                .doc(data));
        try {
            UpdateResponse<?> response = elasticsearchClient.update(request, null);
            log.info("更新文档成功：{}", response);
        } catch (IOException e) {
            log.error("更新文档异常：{}", e.getMessage());
        }
    }

    @Override
    public <T> T getDocById(String index, String id, Class<T> clazz) {
        if (!existsDocById(index, id)) {
            return null;
        }
        GetRequest request = GetRequest.of(re -> re.index(index).id(id));
        try {
            GetResponse<Map> response = elasticsearchClient.get(request, Map.class);
            return JSON.parseObject(JSON.toJSONBytes(response.source()), clazz);
        } catch (IOException e) {
            log.error("查询文档异常：{}", e.getMessage());
        }
        return null;
    }

    @Override
    public Boolean existsDocById(String index, String id) {
        co.elastic.clients.elasticsearch.core.ExistsRequest request = co.elastic.clients.elasticsearch.core.ExistsRequest.of(re -> {
            re.index(index).id(id);
            return re;
        });
        try {
            return elasticsearchClient.exists(request).value();
        } catch (IOException e) {
            log.error("文档判断异常：{}", e.getMessage());
            return false;
        }
    }

    @Override
    public Boolean bulkDoc(String index, List<BulkOperation> bulkOperations) {
        try {
            BulkRequest request = BulkRequest.of(re -> re.index(index).operations(bulkOperations));
            BulkResponse response = elasticsearchClient.bulk(request);
            log.info("批量数据：{}", response);
            return true;
        } catch (IOException e) {
            log.error("批量数据异常：{}", e.getMessage());
            return false;
        }
    }

    @Override
    public <T> List<T> searchAll(String index, Class<T> clazz) {
        SearchRequest request = SearchRequest.of(re -> re.index(index)
                .query(Query.of(q -> q.matchAll(m -> m)))
        );
        try {
            SearchResponse<T> response = elasticsearchClient.search(request, clazz);
            return response.hits().hits().stream().map(Hit::source).toList();
        } catch (IOException e) {
            log.error("查询文档异常：{}", e.getMessage());
        }
        return null;
    }

    @Override
    public <T> List<T> searchQuery(String index, Query query, Class<T> clazz, String... highlightField) {
        SearchRequest request = SearchRequest.of(re -> {
                    SearchRequest.Builder builder = re.index(index)
                            .query(query);
                    if (ObjUtil.isNotNull(highlightField)) {
                        for (String highField : highlightField) {
                            builder.highlight(h -> h.fields(highField, f -> f.preTags("<span style='color:red'>").postTags("</span>")));
                        }
                    }
                    return builder;
                }
        );
        try {
            SearchResponse<Map> response = elasticsearchClient.search(request, Map.class);
            return getResponseList(response, highlightField, clazz);
        } catch (IOException e) {
            log.error("查询文档异常：{}", e.getMessage(), e);
            return null;
        }
    }

    @Override
    @SuppressWarnings("all")
    public <T> IPage<T> searchDoc(String index, Query query, Integer limit, Integer page, SortOptions[] sortField, String[] highlightField, Class<T> clazz, String... fields) {
        SearchRequest request = SearchRequest.of(re -> {
            SearchRequest.Builder builder = re.index(index)
                    .query(query)
                    .from((page - 1) * limit)
                    .size(limit)
                    .sort(Arrays.asList(sortField));
            if (ObjUtil.isNotNull(highlightField)) {
                for (String highField : highlightField) {
                    builder.highlight(h -> h.fields(highField, hf -> hf.preTags("<span style='color:red'>").postTags("</span>")));
                }
            }
            return builder;
        });
        try {
            IPage<T> iPage = new Page<>(page, limit);
            SearchResponse<Map> response = elasticsearchClient.search(request, Map.class);
            List<T> objs = getResponseList(response, highlightField, clazz);
            iPage.setRecords(objs);
            iPage.setSize(limit);
            if (ObjUtil.isNotNull(response.hits().total())) {
                iPage.setTotal(response.hits().total().value());
            }
            return iPage;
        } catch (IOException e) {
            log.error("查询文档异常：{}", e.getMessage(), e);
            return null;
        }
    }

    @SuppressWarnings("all")
    private <T> List<T> getResponseList(SearchResponse<Map> response, String[] highlightField, Class<T> clazz) throws IOException {
        List<Map> objs = new ArrayList<>();
        HitsMetadata<Map> hits = response.hits();
        for (Hit<Map> hit : hits.hits()) {
            Map<String, List<String>> highlight = hit.highlight();
            Map<String, Object> source = hit.source();
            if (ObjUtil.isNotNull(source) && !source.isEmpty()) {
                if (ObjUtil.isNotNull(highlightField)) {
                    Map<String, String> highlightMap = new HashMap<>();
                    for (String highField : highlightField) {
                        if (ObjUtil.isNotNull(highlight) && highlight.containsKey(highField)) {
                            highlightMap.put(highField, highlight.get(highField).get(0));
                        }
                    }
                    source.put("highlight", highlightMap);
                }
            }
            objs.add(source);
        }
        return objs.stream().map(o -> JSON.parseObject(JSON.toJSONString(o), clazz)).toList();
    }
}
